This project is intended as a general python library for dealing with data from the PALS project.

All config is stored in `pals_utils/data/config.py`. To modify e.g. data directories, modify this config in-place, using the set_config functions:

```{python}
from pals_utils.data import set_config


set_config(['dirs', 'met'], '/path/to/met/data')
```

