# -*- coding: utf-8 -*-
"""
Utility functions for loading PALS-style data.

@author: naught101
"""

import os
import glob
import yaml
import xarray as xr
import pandas as pd
import numpy as np
import joblib as jl
import pkg_resources

import logging
logger = logging.getLogger(__name__)


# #######################
# Config
# #######################

pals_data_dir = pkg_resources.resource_filename('pals_utils', 'data')

config_file = os.path.join(pals_data_dir, "config.yaml")
with open(config_file) as f:
    config = yaml.safe_load(f)

config['dirs'].update({k: os.path.expanduser(v) for k, v in config['dirs'].items()})


def get_config(indices=None, c_dict=config):
    if indices is None:
        return c_dict

    if isinstance(indices, str):
        return c_dict[indices]

    if (len(indices) > 1) and isinstance(c_dict[indices[0]], dict):
        return get_config(indices[1:], c_dict[indices[0]])

    else:
        return c_dict[indices[0]]


def set_config(indices, value, c_dict=config):
    if isinstance(indices, str):
        c_dict.update({indices: value})
    elif (len(indices) > 1) and (indices[0] in c_dict) and isinstance(c_dict[indices[0]], dict):
        set_config(indices[1:], value, c_dict[indices[0]])
    else:
        c_dict.update({indices[0]: value})


# #######################
# Classes
# #######################

class MissingDataError(Exception):
    pass


# #######################
# Metadata stuff
# #######################

def remove_suffix(string, suffix):
    """remove suffix from string
    """
    if string.endswith(suffix) and len(suffix) > 0:
        return string[:-len(suffix)]
    else:
        return string


def pals_site_name(site_data, rem_suffix=True, version=False):
    """Get site identifier from xarray data

    :site_data: xarray Dataset representing a PALS dataset
    :suffix: suffix to remove.
    :returns: name/version string

    """
    try:
        # Ozflux style
        name = site_data.attrs['site_name']
    except KeyError:
        try:
            # Old fluxnet style
            name = site_data.attrs['PALS_dataset_name']
        except KeyError:
            raise KeyError("can't find site name in attributes: %s" % site_data.attrs)

    if rem_suffix:
        for suffix in ['Fluxnet', 'OzFlux']:
            name = remove_suffix(name, suffix)

    if version:
        try:
            # Ozflux style
            version = site_data.attrs['site_dataset_version']
        except KeyError:
            try:
                # Old fluxnet style
                version = site_data.attrs['PALS_dataset_version']
            except KeyError:
                raise KeyError("can't find site version in attributes: %s" % site_data.attrs)

        return '%s%s' % (name, version)
    else:
        return name


# ###################
# Metadata functions
# ###################

def get_site_path(site, typ, dataset=None):
    """Return a path for a given site name."""

    if dataset is not None:
        datasets = [dataset]
    else:
        datasets = ['Fluxnet.1.4', 'OzFlux2.0']

    datapath = get_config(['dirs', typ])

    for d in datasets:
        path = '{p}/{s}{d}_{t}.nc'.format(p=datapath, t=typ, s=site, d=d)
        if os.path.isfile(path):
            return path

    # FluxnetLSM output format:
    pattern = '{d}/{s}_{y}-{y}_FLUXNET2015_{t}.nc'.format(d=datapath, s=site, y='[0-9]' * 4, t=typ.capitalize())
    files = glob.glob(pattern)
    if len(files) > 0:
        if len(files) > 1:
            logger.warning("Found more than one {t} file for {s} . Using {f}"
                           .format(t=typ, s=site, f=os.path.basename(files[0])))
        return files[0]

    # Merged output format:
    pattern = '{d}/{s}_{y}-{y}_{t}.nc'.format(d=datapath, s=site, y='[0-9]' * 4, t=typ)
    files = glob.glob(pattern)
    if len(files) > 0:
        if len(files) > 1:
            logger.warning("Found more than one {t} file for {s} . Using {f}"
                           .format(t=typ, s=site, f=os.path.basename(files[0])))
        return files[0]

    raise NameError('Unknown site "{s}"'.format(s=site))


def get_site_code_from_ds(site_ds):
    try:
        return site_ds.site_code
    except AttributeError:
        try:
            site_name = site_ds.site_name
        except AttributeError:
            site_name = site_ds.PALS_dataset_name
            if site_name.endswith("Fluxnet"):
                site_name = site_name[:-7]

        return get_site_code_from_name(site_name)


def get_site_code_from_name(site_name):
    try:  # From known site codes file
        code_file = os.path.join(pals_data_dir, "pals_site_codes.yaml")
        with open(code_file) as f:
            site_code = yaml.load(f)[site_name]
        return site_code
    except KeyError:
        pass

    df = pd.read_csv(os.path.join(pals_data_dir, 'fluxnet_status_table.csv'))

    try:  # from dataframe directly
        fluxnet_id = df.set_index('Site Name').ix[site_name, 'FLUXNET ID']
        return fluxnet_id
    except KeyError:
        pass

    try:  # look up name translation first
        translation_file = os.path.join(pals_data_dir, "site_translations.yaml")
        with open(translation_file) as f:
            translation = yaml.load(f)[site_name]
        fluxnet_id = df.set_index('Site Name').ix[translation, 'FLUXNET ID']
        return fluxnet_id
    except KeyError:
        pass

    logger.error("Unknown site " + site_name + ", add to " + translation_file)


def get_site_code(site):
    logger.warning("Deprecated: use get_site_code_from_ds or get_site_code_from_name directly")

    if isinstance(site, xr.Dataset):
        return get_site_code_from_ds(site)

    elif isinstance(site, str):
        return get_site_code_from_name(site)

    else:
        raise TypeError("Don't know how to handle site information of type " + type(site))


def get_site_lat_lon(site):

    try:
        code = get_site_code(site)
        with open(os.path.join(pals_data_dir, 'fluxnet_info', code + '.yaml')) as f:
            lat_lon = yaml.load(f)['Location Information ']['Coordinates:(Lat, Long)']
            lat, lon = (float(l) for l in lat_lon.split(','))
        return lat, lon
    except FileNotFoundError:
        pass

    try:
        with get_flux_data(site)[site] as ds:
            lat = ds['latitude'].values.ravel()[0]
            lon = ds['longitude'].values.ravel()[0]
        return lat, lon
    except FileNotFoundError:
        pass

    logger.warning("Can't find lat, lon for " + site)
    return (0.0, 0.0)


def get_site_n_years(site):

    try:
        with get_flux_data(site)[site] as ds:
            times = ds.time.values
            years = pd.DatetimeIndex([times[0], times[-1] + np.timedelta64(30, 'm')]).year
        return (years[-1] - years[0])
    except FileNotFoundError:
        pass

    logger.warning("Can't find number of years for " + site)
    return 0.0


def get_site_veg_type(site):
    try:
        with get_flux_data(site)[site] as ds:
            veg_type = ds.attrs['IGBP_vegetation_type']
        return veg_type
    except:
        pass

    try:
        code = get_site_code(site)
        with open(os.path.join(pals_data_dir, 'fluxnet_info', code + '.yaml')) as f:
            veg_type = yaml.load(f)['Site Characteristics ']['IGBP Land Cover']
        return veg_type
    except:
        pass

    logger.warning("Unknown veg type for " + site)
    return "unknown"


# #######################
# Data loading functions
# #######################


def get_data_dir():
    """get data directory """

    return get_config(['dirs', 'data'])


def get_sites(site_set='all'):
    """load names of available sites"""

    if site_set == 'debug':
        sites = ['Tumba']
    else:
        if site_set == 'all':
            filename = 'sites.txt'
        elif site_set == 'PLUMBER':
            filename = 'sites_PLUMBER.txt'
        elif site_set == 'PLUMBER_ext':
            filename = 'sites_PLUMBER_ext.txt'
        elif site_set == 'Fluxnet2015Tier1':
            filename = 'sites_Fluxnet2015Tier1.txt'
        elif site_set == 'Fluxnet2015Tier1_HH':  # Half-hourly only
            filename = 'sites_Fluxnet2015Tier1_HH.txt'
        else:
            raise KeyError("Unknown site set %s" % site_set)

        path = os.path.join(pals_data_dir, filename)
        with open(path) as f:
            sites = [s.strip() for s in f.readlines()]

    return sites


def get_site_data(site_names, typ, datapath=get_config(['dirs', 'data'])):
    """Get a dictionary of xarray datasets from a list of PALS site names"""

    if isinstance(site_names, str):
        site_names = [site_names]

    paths = [get_site_path(site, typ) for site in site_names]
    return dict(zip(site_names, get_site_data_list(paths)))


def get_met_data(site_names, datapath=get_config(['dirs', 'data'])):
    """helper for get_site_data"""
    return get_site_data(site_names, 'met', datapath)


def get_flux_data(site_names, datapath=get_config(['dirs', 'data']), fix_closure=False):
    """helper for get_site_data"""
    data_dict = get_site_data(site_names, 'flux', datapath)
    if fix_closure:
        [fix_energy_closure(ds) for ds in data_dict.values()]
    return data_dict


def get_met_df(site_names, variables=get_config(['vars', 'met']), name=False, qc=False,
               datapath=get_config(['dirs', 'data'])):
    """helper for get_site_data, returns a dataframe"""
    # just use dictionary values
    xr_list = get_met_data(site_names, datapath).values()
    return xr_list_to_df(xr_list, variables=variables, name=name, qc=qc)


def get_flux_df(site_names, variables=get_config(['vars', 'flux']), name=False, qc=False,
                fix_closure=False, datapath=get_config(['dirs', 'data'])):
    """helper for get_site_data, returns a dataframe"""
    # just use dictionary values
    xr_list = get_flux_data(site_names, datapath, fix_closure=fix_closure).values()
    return xr_list_to_df(xr_list, variables=variables, name=name, qc=qc)


if get_config('use_cache'):
    mem = jl.Memory(cachedir=os.path.join(os.path.expanduser('~'), 'tmp', 'cache'))

    get_met_df_cached = mem.cache(get_met_df)
    get_flux_df_cached = mem.cache(get_flux_df)
else:
    get_met_df_cached = get_met_df
    get_flux_df_cached = get_flux_df


def get_data_df(sites, var, name=False, qc=False):
    """load arbitrary data """
    if var in ['SWdown', 'LWdown', 'Tair', 'RelHum', 'Qair', 'Wind', 'Rainf']:
        data = get_met_df(sites, [var], name=name, qc=qc)
    else:
        data = get_flux_df(sites, [var], name=name, qc=qc)

    return data


def get_site_data_list(paths):
    """get a list of xarray datasets from a list of netcdf paths"""
    datasets = [xr.open_dataset(path) for path in paths]

    qc_format = get_config(['qc_format'])

    if qc_format == 'FluxnetLSM':
        pass  # default - 0 == good, anything else is gapfilled.
    elif qc_format == 'PALS':
        # PALS used 1 = good, 0 = bad. So invert.
        for ds in datasets:
            qc_vars = [v for v in list(ds.data_vars) if '_qc' in v]
            for v in qc_vars:
                ds[v] = 1 - ds[v]
    else:
        raise KeyError("Unknown QC format: %s" % qc_format)

    return datasets


def get_multisite_met_df(sites, variables, name=False, qc=False):
    """Load some data and convert it to a dataframe

    :sites: str or list of strs: site names
    :variables: list of variable names
    :qc: Whether to replace bad quality data with NAs
    :names: Whether to include site-names
    :returns: pandas dataframe

    """
    if isinstance(sites, str):
        sites = [sites]

    logger.info("Met data: loading... ")
    df = pd.concat([get_met_df_cached([s], variables, name=name, qc=qc) for s in sites])
    return df


def get_multisite_flux_df(sites, variables, name=False, qc=False, fix_closure=True):
    """Load some data and convert it to a dataframe

    :sites: str or list of strs: site names
    :variables: list of variable names
    :qc: Whether to replace bad quality data with NAs
    :names: Whether to include site-names
    :returns: pandas dataframe

    """
    if isinstance(sites, str):
        sites = [sites]

    logger.info("Flux data: loading... ")
    df = pd.concat([
        get_flux_df_cached([s], variables, name=name, qc=qc, fix_closure=fix_closure)
        for s in sites])
    return df


################
# Data Munging #
################

def fix_energy_closure(ds):

    logger.info("Trying to correct energy closure at %s (%s)" %
                (get_site_code_from_ds(ds), pals_site_name(ds)))

    if 'Rnet' not in ds.data_vars:
        logger.warning('Rnet not found in %s! Not fixing energy closure' % pals_site_name(ds))
        # TODO: Is it worth calculating these from SW/LW net/up/down?
        return

    good_data = (ds['Rnet_qc'] + ds['Qh_qc'] + ds['Qle_qc']).values == 0

    if good_data.sum() < 182 * 48:
        logger.warning('Using less than half a year of data to fix energy closure at {s}! {g}/{t}'.format(
            s=pals_site_name(ds), g=good_data.sum(), t=len(ds.time)))
    if good_data.mean() < 0.25:
        logger.warning('Using less than 25%% of data to fix energy closure at {s}! {g}/{t}'.format(
            s=pals_site_name(ds), g=good_data.sum(), t=len(ds.time)))

    scale_ratio = (ds.data_vars['Rnet'].values[good_data].sum() /
                   (ds.data_vars['Qh'].values[good_data].sum() +
                    ds.data_vars['Qle'].values[good_data].sum()))

    # Fix energy fluxes in-place
    ds['Qh'] = scale_ratio * ds['Qh']
    ds['Qle'] = scale_ratio * ds['Qle']

    return


##############
# Benchmarks #
##############

def get_pals_benchmark(name, site, datapath=get_config(['dirs', 'data'])):
    """Load a benchmark from a pre-processed PLUMBER benchmark file.

    :name: benchmark name
    :site: PALS site name
    :returns: TODO
    """
    if name not in ['1lin', '2lin', '3km27']:
        raise Exception('Unknown benchmark: %s' % name)

    file_path = datapath + '/benchmarks/{name}/{name}_{site}Fluxnet.1.4.nc'

    benchmark = xr.open_dataset(file_path.format(name=name, site=site))

    return benchmark


def get_pals_benchmark_df(name, sites, variables, datapath=get_config(['dirs', 'data'])):
    """Load a benchmark from multiple sites, and convert to a dataframe"""
    data = {}
    for s in sites:
        try:
            ds = get_pals_benchmark(name, s)
            data[s] = (ds[variables]
                       .sel(x=1, y=1)[variables]
                       .to_dataframe()[variables])
            ds.close()
        except Exception as e:
            logger.warning('Skipping', s, ':', e)

    return pd.concat(data, names=['site'])


def get_pals_old_benchmark(name, site, datapath=get_config(['dirs', 'data'])):
    """Load a benchmark from a PALS benchmark file

    :name: benchmark name
    :site: PALS site name
    :returns: TODO
    """
    if name not in ['1lin', '2lin', '3km27']:
        raise Exception('Unknown benchmark: %s' % name)

    geo_vars = ['latitude', 'longitude']

    filename = '{p}/benchmarks/1.4/{s}Fluxnet_1.4_PLUMBER_benchmarks.nc'.format(p=datapath, s=site)

    with xr.open_dataset(filename) as dataset:
        data_vars = [v for v in list(dataset.data_vars) if name in v]
        benchmark = dataset[geo_vars + data_vars].copy(deep=True)

    new_var_names = geo_vars + [v.rstrip('_' + name) for v in data_vars]
    var_name_dict = dict(zip(geo_vars + data_vars, new_var_names))

    benchmark.rename(var_name_dict, inplace=True)

    return benchmark


# #######################
# Simplifiers
# #######################

def load_nc_to_dataframe(filename, variables, name=False, qc=False):
    """Loads dat for multiple variables a non-gridded netCDF file

    :param filename: filename of the nc file to be opened
    :param variables: list of names of the variables to load
    :rtype: `pandas.DataFrame`
    """
    with xr.open_dataset(filename) as dataset:
        return pals_xr_to_df(dataset, variables=variables, name=name, qc=qc)


def load_nc_var(filename, variable):
    """Loads data for a single variable from a non-gridded netCDF

    :param filename: filename of the nc file to be opened
    :param variable: name of the variable to load
    :rtype: `numpy.ndarray`
    """
    with xr.open_dataset(filename) as dataset:
        data = dataset.data_vars[variable].values.flatten().copy()
    return data


# ##########################
# Data format conversion
# ##########################

def pals_xr_to_array(dataset, variables, name=False, qc=False):
    """Wrapper around pals_xr_to_df

    :returns: 2D array of variables
    """
    return pals_xr_to_df(dataset, variables=variables, name=name, qc=qc).as_matrix()


def pals_xr_add_relhum(ds):
    missing_vars = [v for v in ['Tair', 'Qair', 'PSurf'] if v not in list(ds.data_vars)]
    if len(missing_vars) == 0:
        ds['RelHum'] = Spec2RelHum(ds['Qair'], ds['Tair'], ds['PSurf'])
        ds['RelHum_qc'] = np.fmax(ds['Tair_qc'], ds['Qair_qc'])  # worst qc
    else:
        logger.warning('Missing {v} - RelHum: filled with NA'.format(v=missing_vars))
        ds['RelHum'] = np.nan * ds['SWdown']
        ds['RelHum_qc'] = 0 * ds['SWdown_qc'] + 7  # arbitrary..


def pals_xr_add_delta(ds, variables):
    # TODO: Currently assumes only one site
    if 'deltaQ' in variables:
        if 'Qair' not in ds:
            ds['Qair'] = np.nan * ds['SWdown']
            ds['Qair_qc'] = 0 * ds['SWdown_qc'] + 7
        deltaQ = np.full_like(ds['Qair'].values, np.nan)
        # TODO: Initialisation here is a bit weak.
        deltaQ.ravel()[0] = 0
        qair = ds['Qair'].values.ravel()
    if 'deltaT' in variables:
        deltaT = np.full_like(ds['Tair'].values, np.nan)
        deltaT.ravel()[0] = 0
        tair = ds['Tair'].values.ravel()

    swdown = ds['SWdown'].values.ravel()
    cur_sw = swdown[0]
    sunrise = 0

    for i in range(1, len(ds['SWdown'])):
        prev_sw = cur_sw
        cur_sw = swdown[i]

        sw_24h = swdown[max(0, i - 24):i]
        sw_min_24h = sw_24h.min()

        if prev_sw < 5 and cur_sw >= 5:
            # Sunrise!
            sunrise = i
        elif sw_min_24h >= 5:
            # Midnight sun: dawn is the darkest time
            sunrise = i - (24 - sw_24h.argmin())

        if 'deltaQ' in variables:
            deltaQ.ravel()[i] = qair[i] - qair[sunrise]
        if 'deltaT' in variables:
            deltaT.ravel()[i] = tair[i] - tair[sunrise]

    if 'deltaQ' in variables:
        ds['deltaQ'] = xr.DataArray(data=deltaQ, coords=ds['Qair'].coords, dims=ds['Qair'].dims)
        # TODO: This QC is not good enough, needs to include 24 hour lag for SWdown and Qair
        logger.warning("TODO: QC flags for deltaQ are inadequate. See pals_utils/data.py:301")
        ds['deltaQ_qc'] = ds['Qair_qc']
    if 'deltaT' in variables:
        ds['deltaT'] = xr.DataArray(data=deltaT, coords=ds['Tair'].coords, dims=ds['Tair'].dims)
        logger.warning("TODO: QC flags for deltaT are inadequate. See pals_utils/data.py:301")
        ds['deltaT_qc'] = ds['Tair_qc']


def pals_xr_to_df(dataset, variables, name=False, qc=False):
    """Extract a dataframe from a PALS-style xarray Dataset.

    TODO: figure out what to do about gridded datasets

    :dataset: PALS-style xr.Dataset
    :variables: flux/met/meta variables to include (will only be included if they exist in the first place).
    :returns: TODO
    """

    if 'RelHum' in variables:
        if 'RH' in dataset.data_vars:
            dataset.rename({"RH": "RelHum", "RH_qc": "RelHum_qc"}, inplace=True)
        else:
            pals_xr_add_relhum(dataset)

    delta_vars = [v for v in ['deltaT', 'deltaQ'] if v in variables]
    if len(delta_vars) > 0:
        pals_xr_add_delta(dataset, delta_vars)

    data_vars = [v for v in variables if v in list(dataset.data_vars)]
    if len(data_vars) < 1:
        raise MissingDataError("No required variables found in dataset {s}"
                               .format(s=pals_site_name(dataset)))

    if 'time_counter' in dataset.coords:
        # Work around for ORCHIDEE
        dataset = dataset.rename({'time_counter': 'time'})

    index_vars = list(dataset.coords)
    if 'time' in index_vars:
        index_vars.remove('time')

    # TODO: This is not suitable for gridded datasets:
    try:
        index_vars = {v: dataset.coords[v].values[0] for v in index_vars}
        df = dataset.sel(**index_vars)[data_vars].to_dataframe()[data_vars]
    except ValueError:
        # Some PALS datasets have weird coords.. some kind of work-around.
        df = dataset.sel(x=0, y=0)[data_vars].to_dataframe()[data_vars]

    # More coord weirdness fixing
    to_drop = [l for l in ['x', 'y'] if l in df.index.names]
    if len(to_drop) > 0:
        df.index = df.index.droplevel(to_drop)

    if qc:
        for v in data_vars:
            if v + '_qc' not in list(dataset.data_vars):
                logger.warning('QC flags missing for %s at %s. Assuming bad.' % (v, pals_site_name(dataset)))
                dataset[v + '_qc'] = dataset['SWdown_qc'] * 0 + 7  # arbitrary fill
        qc_vars = [v + '_qc' for v in data_vars]
        qc_df = dataset.sel(**index_vars)[qc_vars].to_dataframe()[qc_vars]
        qc_mask = qc_df.as_matrix() == 0
        df = df[data_vars].where(qc_mask)

    if name:
        df['site'] = pals_site_name(dataset)
        df.set_index('site', append=True, inplace=True)

    missing_vars = [v for v in variables if v not in data_vars]
    for v in missing_vars:
        df[v] = np.nan

    return df


def xr_list_to_df(xr_list, variables, name=False, qc=False):
    df_list = []
    for ds in xr_list:
        try:
            df_list.append(pals_xr_to_df(ds, variables=variables, name=name, qc=qc))
        except MissingDataError as e:
            logger.warning(e)

    df = pd.concat(df_list)
    df.columns.name = 'variable'
    return df


# ##########################
# Dataset creation
# ##########################

def copy_data(dataset):
    """Return a copy of the land dataset metadata without any data.
    """
    geo_vars = []
    for v in get_config(['vars', 'geo']):
        if v in dataset:
            geo_vars.append(v)
        else:
            try:
                name = pals_site_name(dataset)
                logger.warning('{v} missing from {s}'.format(v=v, s=name))
            except KeyError:
                logger.warning('Unknown site, copying available data')

    ds = dataset[geo_vars]

    for c in dataset.coords:
        if c not in ds.coords:
            ds.coords[c] = dataset.coords[c]

    # Clear out attrs
    ds.attrs = {}

    return ds


# #########################
# Data manipulation helpers
# #########################

def time_split(dataset, ratio):
    """Split data along the time axis, by ratio"""
    first_len = np.floor(ratio * dataset.dims['time'])
    first = dataset[dataset['time'] < dataset['time'][first_len]]
    second = dataset[dataset['time'] >= dataset['time'][first_len]]
    return first, second

# ######################
# Dataset transforms
# ######################

# From palsR


def Rel2SpecHum(RelHum, TairK, PSurf):
    """Converts relative humidity to specific humidity.

    TairK - T in Kelvin
    PSurf in Pa
    RelHum as %"""

    zeroC = 273.15

    tempC = TairK - zeroC
    # Sat vapour pressure in Pa
    esat = 610.78 * np.exp(17.27 * tempC / (tempC + 237.3))
    # Then specific humidity at saturation:
    ws = 0.622 * esat / (PSurf - esat)
    # Then specific humidity:
    SpecHum = (RelHum / 100) * ws

    return SpecHum


def Spec2RelHum(SpecHum, TairK, PSurf):
    """Converts relative humidity to specific humidity.

    TairK - T in Kelvin
    PSurf in Pa
    RelHum as %"""

    zeroC = 273.15

    tempC = TairK - zeroC
    # Sat vapour pressure in Pa
    esat = 610.78 * np.exp(17.27 * tempC / (tempC + 237.3))
    # Then specific humidity at saturation:
    ws = 0.622 * esat / (PSurf - esat)
    # Then relative humidity:
    RelHum = np.clip(SpecHum / ws * 100, 0, 100)

    return RelHum
