#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
File: tests.py
Author: naughton101
Email: naught101@email.com
Github: https://github.com/naught101/
Description: Test difficult PALS util functions
"""

import unittest
import numpy as np

from .. import data as pud


class TestRelHum(unittest.TestCase):
    """Test RelHum"""

    def setUp(self):
        self.X = [ds.isel(time=slice(0, 48)) for ds in
                  pud.get_site_data(['Amplero', 'Tumba'], 'met').values()]

    def tearDown(self):
        pass

    def test_RelHum_convert(self):

        variables = ['SWdown', 'RelHum']

        X_df = pud.xr_list_to_df(self.X, variables, qc=True, name=True)

        self.assertFalse(np.isnan(X_df['RelHum'].values[0]))


class TestDataHandling(unittest.TestCase):
    """Test RelHum"""

    def setUp(self):
        self.sites = ['Amplero', 'Tumba']
        self.variables = ['SWdown', 'RelHum']

    def tearDown(self):
        pass

    def test_Data_loading(self):

        df = pud.get_met_df(self.sites, self.variables)

        self.assertGreater(len(df), 0)
        print(df)
