#!/bin/env python

import netCDF4
import glob
import os
import warnings
import numpy
import pandas
import datetime

import logging
logger = logging.getLogger(__name__)


def get_data_paths(path):
        paths = {}
        paths['obs'] = path + "datasets/"
        paths['obs_flux'] = paths['obs'] + "flux/"
        paths['obs_met'] = paths['obs'] + "met/"

        paths['sim'] = path + "modeloutputs/"
        paths['sim_metadata'] = paths['sim'] + "meta/PALS_model_output.csv"
        paths['sim_variable_metadata'] = paths['sim'] + "meta/PALS_variable_metadata.csv"
        return(paths)


# list all data
def list_nc_files(path=None):
    return(glob.glob(path + '*.nc'))


def get_all_simulation_metadata(path, metadata_path):
    data_sets = list_nc_files(path)

    metadata_all = pandas.DataFrame.from_csv(metadata_path)

    metadata_files = {}
    for filename in data_sets:
        id = os.path.basename(filename).lstrip('mo').rstrip('.nc')
        metadata_files[id] = {"filename": filename}

    metadata_files = pandas.DataFrame.from_dict(metadata_files, orient='index')
    metadata_all.simulationID = metadata_all.simulationID.apply(str)
    metadata = pandas.concat([metadata_files, metadata_all.set_index('simulationID')], axis=1, verify_integrity=True)

    return(metadata)


def get_all_obs_metadata(path):
    datasets = list_nc_files(path)
    metadata = {}
    for filename in datasets:
        metadata.update(get_obs_metadata(filename))
    return(metadata)


def get_obs_metadata(filename):
    basename = os.path.basename(filename)
    version = "Fluxnet.1.4"
    data = basename.split("_")
    file_id = data[0]
    place = file_id.rstrip(version)
    file_type = data[1].rstrip(".nc")
    metadata = {file_id: {"place": place,
                          "version": version,
                          "filetype": file_type,
                          "filename": filename,
                          }}
    return(metadata)


def list_nc_variables(file_path):
    f = netCDF4.Dataset(file_path, "r")
    variables = {}
    try:
        for v in f.variables:
            variables[v] = {}
            for attr in f.variables[v].ncattrs():
                variables[v][attr] = f.variables[v].getncattr(attr)
    except Exception as e:
        logger.warning("no variables: ", e)
    finally:
        f.close()
        return(variables)


def get_nc_timestep(file_path):
    f = netCDF4.Dataset(file_path, "r")
    try:
        time = f.variables['time']
        # probably should ensure that this is in seconds
        dt = time[2] - time[1]
    except Exception as e:
        logger.warning("no time variables: ", e)
        dt = False
    finally:
        f.close()
        return(dt)


def load_nc_variable(file_path, variable):
    f = netCDF4.Dataset(file_path, "r")
    try:
        return(f.variables[variable])
    except Exception as e:
        logger.warning("no matching variable: ", e)
        pass


def nc_to_DataFrame(file_path, variables=None, offset=None, limit=None):
    """
    Gets variables from a netcdf file, and returns them as a pandas dataframe, indexed by time.

    Currently assumes that variables are one dimensional
    """
    f = netCDF4.Dataset(file_path, "r")
    if variables is None:
        variables = list(set(f.variables).intersection(set(nc_vars_all)))
    data_dict = {}
    # There surely should be a way to extract the time from the ncdf files?
    time = f.variables['time'][:].flatten()
    time_units = f.variables['time'].units.split(' since ')
    start_date = datetime.datetime.strptime(time_units[1], '%Y-%m-%d %H:%M:%S')
    time = [start_date + datetime.timedelta(seconds=int(s)) for s in time]

    if offset is None:
        start, end = 0, limit
    else:
        if limit is None:
            start, end = offset, None
        else:
            start, end = offset, limit + offset

    for var in variables:
        try:
            assert numpy.prod(f.variables[var].shape) == max(f.variables[var].shape)
            data_dict[var] = pandas.Series(f.variables[var][:].flatten(), index=time)[start:end]  # This will break if the end is too large
        except KeyError as e:
            warnings.warn('Unknown variable %s. Skipped.' % (var))
        except AssertionError as e:
            warnings.warn('Variable %s is not one dimensional! Skipped.' % (var))

    dataframe = pandas.DataFrame(data_dict)
    # This seems fairly inflexible, but I don't know of a more general way
    dataframe[dataframe <= -970] = numpy.NaN
    return(dataframe)


nc_vars_meta = ['elevation', 'latitude', 'longitude', 'reference_height',
                'time', 'x', 'y']

nc_vars_obs_flux = ['GPP', 'GPP_qc', 'NEE', 'NEE_qc', 'Qg', 'Qg_qc', 'Qh', 'Qh_qc', 'Qle', 'Qle_qc', 'Rnet', 'Rnet_qc', 'elevation', 'latitude', 'longitude', 'reference_height', 'time', 'x', 'y']

nc_vars_obs_met = ['LWdown', 'LWdown_qc', 'PSurf', 'PSurf_qc', 'Qair', 'Qair_qc',
                   'Rainf', 'Rainf_qc', 'SWdown', 'SWdown_qc', 'Tair', 'Tair_qc',
                   'Wind', 'Wind_qc',
                   'elevation', 'latitude', 'longitude', 'reference_height',
                   'time', 'x', 'y', 'z']

nc_vars_simulation = ['ACond', 'Albedo', 'AvgSurfT', 'BareSoilT', 'CCond', 'CO2air',
                      'CanopInt', 'DelColdCont', 'DelIntercept', 'DelSWE',
                      'DelSoilHeat', 'DelSoilMoist', 'DelSurfStor', 'ECanop',
                      'ESoil', 'EWater', 'Evap', 'EvapSnow', 'HSoil', 'HVeg',
                      'LWdown', 'LWnet', 'Latitude', 'Longitude', 'NEE', 'PSurf',
                      'PotEvap', 'Qa', 'Qair', 'Qf', 'Qfz', 'Qg', 'Qh', 'Qle', 'Qs',
                      'Qsb', 'Qsm', 'Qst', 'Qv', 'RadT', 'RadTmax', 'RadTmin',
                      'Rainf', 'Rnet', 'RootMoist', 'SWE', 'SWdown', 'SWnet',
                      'SnowFrac', 'SnowT', 'Snowf', 'SoilMoist', 'SoilTemp',
                      'SoilWet', 'SubSnow', 'SubSurf', 'SurfStor', 'TVeg', 'Tair',
                      'VegT', 'WaterTableD', 'Wind', 'albsoil', 'bch', 'canst1',
                      'clay', 'css', 'dleaf', 'ejmax', 'extkn', 'frac4', 'froot',
                      'hc', 'hyds', 'isoil', 'iveg', 'lat', 'latitude', 'level',
                      'lon', 'longitude', 'meth', 'ratecp', 'ratecs', 'rhosoil',
                      'rp20', 'rpcoef', 'rs20', 'sand', 'sfc', 'shelrb', 'silt',
                      'ssat', 'sucs', 'swilt', 'time', 'time_bounds', 'timestep',
                      'tmaxvj', 'tminvj', 'vbeta', 'vcmax', 'vegcf', 'wai', 'x',
                      'xalbnir', 'xfang', 'y', 'za', 'zse']

nc_vars_shared_flux = list(set(nc_vars_obs_flux).intersection(set(nc_vars_simulation))
                                                .difference(set(nc_vars_meta)))

nc_vars_shared_met = list(set(nc_vars_obs_met).intersection(set(nc_vars_simulation))
                                              .difference(set(nc_vars_meta)))

nc_vars_all = list(set(nc_vars_shared_flux).union(set(nc_vars_shared_met)))

nc_vars_simulation_missing = list(set(nc_vars_simulation).difference(set(nc_vars_obs_flux))
                                                         .difference(set(nc_vars_obs_met)))
