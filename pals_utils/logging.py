#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
File: logging.py
Author: naught101
Email: naught101@email.com
Github: https://github.com/naught101/empirical_lsm
Description: basic logging functions for scripts
"""

import os
import coloredlogs
import logging
import datetime


def add_filehandler(logger, logname):
    dirname = os.path.dirname(logname)
    os.makedirs(dirname, exist_ok=True)

    logname, ext = os.path.splitext(logname)
    timestamp = datetime.datetime.now().strftime("%Y.%m.%d-%H.%M.%S")

    filename = logname + '_' + timestamp + ext

    filehandler = logging.FileHandler(filename)
    filehandler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s %(name)s: %(message)s')
    filehandler.setFormatter(formatter)

    logger.addHandler(filehandler)

    return filehandler


def setup_logger(name, logname):
    logger = logging.getLogger(name)

    add_filehandler(logger, logname)

    logger.setLevel(logging.NOTSET)

    return logger


stdout = logging.StreamHandler()
stdout.setLevel(logging.INFO)
formatter = coloredlogs.ColoredFormatter('%(levelname)s: %(message)s')
stdout.setFormatter(formatter)

root_logger = logging.getLogger()
root_logger.addHandler(stdout)
