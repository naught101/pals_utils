# -*- coding: utf-8 -*-
"""
Utility functions for plotting PALS-style data.

@author: naught101
"""


# #######################
# Plotting helpers
# #######################

def plot_test_data(Y_pred, Y_validate, y_var):
    # Sample plot
    plot_data = pd.DataFrame({y_var + "_obs": Y_validate, y_var + "_pred": Y_pred})

    # week 7 raw
    pl.plot(plot_data[(70 * 48):(77 * 48)])
    pl.legend(plot_data.columns)
    pl.show()

    # fornightly rolling mean
    pl.plot(pd.rolling_mean(plot_data, window=14 * 48))
    pl.legend(plot_data.columns)
    pl.show()

    # daily cycle
    pl.plot(plot_data.groupby(np.mod(plot_data.index, 48)).mean())
    pl.legend(plot_data.columns)
    pl.show()


def title_to_filename(title):
    """generate a plot filename from a plot title

    :param title: title string.
    :rtype: `str`
    """
    filename = 'plots/{0}.png'.format(title)

    return filename.replace(' ', '_')
