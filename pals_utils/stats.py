# -*- coding: utf-8 -*-
"""
Statistical helper functions useful for analysing PALS-style data.

@author: naught101
"""

import scipy as sp
import numpy as np

from sklearn import cluster as skc

from collections import OrderedDict

import logging
logger = logging.getLogger(__name__)


# Information theory related helper functions

def shannon_entropy(x, unit='bit'):
    """Calucates the Shannon entropy (H(x)).

    See https://en.wikipedia.org/wiki/Information_entropy#Definition

    units can be one of 'bit', 'nit', or 'dit'
    """

    try:
        log = {'bit': np.log2, 'nit': np.log, 'dit': np.log10}[unit]
    except KeyError:
        raise Exception('unknown unit %s' % (unit))

    P = sp.stats.kde.gaussian_kde(x)
    H = - sum(P(x) * log(P(x)))

    return H


def mutual_information(x, y, unit='bit'):
    """Calculates the continuous mutual information of two vectors.

    Based on https://en.wikipedia.org/wiki/Mutual_information and
    http://stackoverflow.com/questions/8363085/continuous-mutual-information-in-python
    """

    try:
        log = {'bit': np.log2, 'nit': np.log, 'dit': np.log10}[unit]
    except KeyError:
        raise Exception('unknown unit %s' % (unit))

    p_x = sp.stats.kde.gaussian_kde(x)
    p_y = sp.stats.kde.gaussian_kde(y)
    p_xy = sp.stats.kde.gaussian_kde([x, y])

    def info(a, b):
        info = p_xy([a, b]) * log(p_xy([a, b]) / (p_x(a) * p_y(b)))
        return info

    x_range = (np.mean(x) - 2 * (max(x) - min(x)), np.mean(x) + 2 * (max(x) - min(x)))
    y_range = (np.mean(y) - 2 * (max(y) - min(y)), np.mean(y) + 2 * (max(y) - min(y)))

    info = sp.integrate.dblquad(info, x_range[0], x_range[1], lambda a: y_range[1], lambda a: y_range[1])

    return info


def OLS_slope_coef(df, xcol=0, ycol=1):
    """Gets the slope coefficient of the slope of the y variable predicted by x

    df: data frame
    xcol: column name of predictor variables
    ycol: column name of predicted variable"""
    x = df.ix[:, xcol]
    y = df.ix[:, ycol]
    slope, intercept, r, p, stderr = sp.stats.linregress(x, y)
    return slope


def RMSD(df, xcol=0, ycol=1):
    """Gets the Root Mean Squared Difference between y and x

    df: data frame
    xcol: column name of first variable
    ycol: column name of second variable"""
    return np.sqrt(np.mean((df.ix[:, xcol] - df.ix[:, ycol]) ** 2))


#########################
# clustering
#########################

def cluster(data, variables, normalise=True,
            model=skc.MiniBatchKMeans(25)):
    """Cluster data in a dataframe, and return the clusters labels as a vector

    :param data: `pandas.DataFrame`
    :param variables: name of the variable
    :param model: `sklearn.model`
    :rtype: `numpy.ndarray` (1D)
    """
    features = data[variables]

    # TODO: replace with pipelines
    if normalise:
        features = (features - features.mean(axis=0)) / features.std(axis=0, ddof=1)

    results = model.fit(np.array(features))

    clusters = results.labels_
    centres = results.cluster_centres_

    return clusters, centres


##########################
# METRICS
##########################

def rmse(pred, obs):
    return np.sqrt(np.mean((obs - pred)**2))


def nme(pred, obs):
    '''PLUMBER normalised mean error'''
    return np.sum(np.abs(obs - pred)) / np.sum(np.abs(obs - np.mean(obs)))


def mbe(pred, obs):
    '''PLUMBER mean bias error'''
    return np.sum(pred - obs) / len(obs)


def sd_diff(pred, obs):
    '''PLUMBER standard deviation difference'''
    return abs(1 - np.std(pred) / np.std(obs))


def corr(pred, obs):
    '''Correlation'''
    return np.corrcoef(pred, obs)[0, 1]


def extreme_5(pred, obs):
    '''PLUMBER 5th percentile difference'''
    return np.abs(np.percentile(pred, 5) - np.percentile(obs, 5))


def extreme_95(pred, obs):
    '''PLUMBER 95th percentile difference'''
    return np.abs(np.percentile(pred, 95) - np.percentile(obs, 95))


def skewness(pred, obs):
    '''PLUMBER skewness'''
    obs_skewness = sum(((obs - np.mean(obs)) / np.std(obs))**3) / len(obs)
    pred_skewness = sum(((pred - np.mean(pred)) / np.std(pred))**3) / len(pred)
    return abs(1 - pred_skewness / obs_skewness)


def kurtosis(pred, obs):
    '''PLUMBER kurtosis'''
    obs_kurtosis = (sum(((obs - np.mean(obs)) / np.std(obs))**4) - 3) / len(obs)
    pred_kurtosis = (sum(((pred - np.mean(pred)) / np.std(pred))**4) - 3) / len(pred)
    return abs(1 - pred_kurtosis / obs_kurtosis)


def overlap(pred, obs, bins=100):
    '''PDF overlap

    Perkins, S. E., A. J. Pitman, N. J. Holbrook, and J. McAneney 2007,
    Evaluation of the AR4 Climate Models’ Simulated Daily Maximum Temperature,
    Minimum Temperature, and Precipitation over Australia Using Probability
    Density Functions. Journal of Climate 20(17): 4356–4376.
    http://journals.ametsoc.org/doi/abs/10.1175/JCLI4253.1
    '''
    # TODO: How are the bins calculated in PLUMBER I?
    bin_range = (min(min(obs), min(pred)), max(max(obs), max(pred)))
    obs_pdf, _ = np.histogram(obs, bins, range=bin_range)
    pred_pdf, _ = np.histogram(pred, bins, range=bin_range)
    return np.sum(np.minimum(obs_pdf, pred_pdf)) / len(obs)


metrics = OrderedDict()
metrics.update({'rmse': rmse})
metrics.update({'nme': nme})
metrics.update({'mbe': mbe})
metrics.update({'sd_diff': sd_diff})
metrics.update({'corr': corr})
metrics.update({'extreme_5': extreme_5})
metrics.update({'extreme_95': extreme_95})
metrics.update({'skewness': skewness})
metrics.update({'kurtosis': kurtosis})
metrics.update({'overlap': overlap})


def run_metrics(pred, obs, metrics=metrics):
    '''Run all of the standard PLUMBER metrics'''
    metric_data = OrderedDict()
    for (name, metric) in metrics.items():
        try:
            metric_data[name] = metric(pred, obs)
        except Exception as e:
            logger.warning('Metric %s failed, using NaN (%s: %s)' % (name, type(e).__name__, e))
            metric_data[name] = np.nan
    return metric_data
