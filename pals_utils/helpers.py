# -*- coding: utf-8 -*-
"""
Utility functions for transforming PALS-style data.

@author: naught101
"""

import numpy as np
import time
import joblib


def timeit(f):
    """Decorator for running a function and timing it.

    :param f: function to time
    :return: function value, time
    """
    def timed(*args, **kw):
        ts = time.time()
        result = f(*args, **kw)
        te = time.time()
        # print(f.__name__, "took: {:2.4f} sec".format(te-ts))
        return result, te - ts
    return timed


def short_hash(obj, n=7):
    """returns a shortened joblib.hash

    :obj: Elements to hash
    :n: number of hash characters to return
    :returns: short hash

    """
    return joblib.hash(obj)[0:n]


def chunks(vec, chunk_size=12):
    """Returns a vector split into a vec of chunks of size chunk_size"""
    return [vec[i:i + chunk_size] for i in range(0, len(vec), chunk_size)]


def vector_autocorrelation_chunks(vector, chunk_size=12):
    """Returns a list of length len(vector)/chunk_size of lists of length
    chunk_size, of tuples (x,y)"""
    return [np.array([vector[i:i + chunk_size - 1], vector[(i + 1):(i + chunk_size)]]) for i in range(0, len(vector), chunk_size)]
