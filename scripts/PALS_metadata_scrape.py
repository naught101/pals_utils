#!/bin/env python3

# Scrapes PALS model output tables for metadata.

import os
import re
import glob
import lxml

from argparse import ArgumentParser

import pandas as pd
import pals_utils.munge.PALS_data as PD

from pals_utils.logging import setup_logger
logger = setup_logger(__name__, "logs/PALS_metadata_scrape.log")


def get_PALS_model_output_html():
    """
    STUB: This should get the html files and cache them, but I don't
    want to screw around with log in cookies. So just save them yourself.
    """
    files = glob.glob('model_outputs_page_*.html')

    return(files)


def scrape_PALS_model_output_html(file_names):
    data = []

    for file_name in file_names:
        with open(file_name) as f:
            PALS_model_output_html = lxml.html.document_fromstring(f.read())

        page = file_name.lstrip('model_outputs_page_').rstrip('.html')

        tables = [parse_html_table(table) for table in PALS_model_output_html.cssselect('.main table')]
        # There might be a better identifier for which table we're in, such as the preceeding <h2>.
        for i in range(len(tables)):
            for row in tables[i]:
                row['page'] = page
                data += [row]

    return(pd.DataFrame(data))


def parse_html_table(table, first_tr_header=True):
    """Parse an HTML table element, and return a list of dicts."""

    if (first_tr_header):
        header = parse_html_table_header(table.cssselect('tr')[0])
        rows = table.cssselect('tr')[1:]
    else:
        header = parse_html_table_header(table.cssselect('th'))
        rows = table.cssselect('tr')

    row_list = []
    for r in rows:
        row_list += [parse_html_table_row(r, header)]

    return(row_list)


def parse_html_table_header(row):
    """STUB: Just returns the correct row titles, should scrape the table"""
    return({0: "simulationID",
            1: "simulationName",
            2: "plotLink",
            3: "username",
            4: "modelName",
            5: "obsDataset",
            6: "date",
            7: "status",
            8: "access",
            9: "downloadLink"})


def parse_html_table_row(row, header):
    cells = row.getchildren()
    values = {}

    # this gets the simulation id from the checkbox
    values[header[0]] = cells[0].getchildren()[0].attrib['value']

    if (cells[9].getchildren()[0].tag == 'a'):
        values[header[9]] = cells[9].getchildren()[0].attrib['href']

    for i in [1, 3, 4, 5, 6, 7, 8]:
        values[header[i]] = cells[i].text_content()

    return({key: re.sub('\s+', ' ', value.strip()) for key, value in values.items()})


def main(data_path=os.path.join(os.path.dirname(__file__), "../data/")):
    # Scrape each of the three inputs, format them to dataframes, and save them to files
    PALS_model_output = scrape_PALS_model_output_html(file_names=get_PALS_model_output_html())
    logger.info("Scraped metadata from HTML files")

    # Load all the metadata!
    paths = PD.get_data_paths(data_path)
    files_obs_met = PD.get_all_obs_metadata(paths['obs_met'])
    files_obs_flux = PD.get_all_obs_metadata(paths['obs_flux'])

    vars_required = ['Rnet', 'SWdown', 'Tair', 'Qair', 'Rainf', 'Wind', 'Qle', 'Qh', 'NEE', 'LWnet', 'SWnet']
    Var_metadata = {}
    logger.info(PALS_model_output.head(1))
    for ind in PALS_model_output.index:
        basename = 'mo' + PALS_model_output.loc[ind, 'simulationID'] + '.nc'
        filename = '../' + basename
        PALS_model_output.ix[ind, 'basename'] = basename
        try:
            # Need to put the file in the models/metadata directory for now. Need to improve this.
            file_vars = PD.list_nc_variables(filename)
            dt = PD.get_nc_timestep(filename)
        except RuntimeError as e:
            logger.warning(e)
            logger.warning('file ' + filename + ' not found.')
            PALS_model_output.loc[ind, 'missing'] = True
            continue

        PALS_model_output.loc[ind, 'obsExistsMet'] = PALS_model_output.loc[ind, 'obsDataset'] in files_obs_met
        PALS_model_output.loc[ind, 'obsExistsFlux'] = PALS_model_output.loc[ind, 'obsDataset'] in files_obs_flux
        PALS_model_output.loc[ind, 'timestep'] = dt

        for var in vars_required:
            PALS_model_output.loc[ind, 'var' + var] = var in file_vars

        Var_metadata.update(file_vars)

    # Might want more nuance (e.g. LWnet+SWnet = Rnet)
    logger.info(PALS_model_output.head(1))
    PALS_model_output['good'] = all(PALS_model_output[['varRnet', 'varTair', 'varQair', 'varRainf', 'varWind']])

    PALS_model_output.to_csv('PALS_model_output.csv', index_label='rowID')
    logger.info("Saved PALS model output metadata.")
    pd.DataFrame.from_dict(Var_metadata, orient='index').to_csv('PALS_variable_metadata.csv', index_label='variable')
    logger.info("Saved PALS model output variable metadata metadata.")

    return


if (__name__ == '__main__'):
        # Script arguments
        parser = ArgumentParser()
        parser.add_argument('path', help='Parent path of model and obs data')

        args = parser.parse_args()

        main(args.path)
