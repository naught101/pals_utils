#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
File: Fluxnet_metadata_scrape.py
Author: naught101
Email: naught101@email.com
Github: https://github.com/naught101/
Description: Scrapes data for sites from http://fluxnet.ornl.gov/site_status

Usage:
    Fluxnet_metadata_scrape.py get_table [--csv-file=<file>]
    Fluxnet_metadata_scrape.py get_site <site_id> [--csv-file=<file>] [--info-dir=<dir>]
    Fluxnet_metadata_scrape.py get_all_sites [--csv-file=<file>] [--info-dir=<dir>]
    Fluxnet_metadata_scrape.py (-h | --help | --version)

Options:
    -h, --help        Show this screen and exit.
    --csv-file=<file> Path to CSV file to save data  [default: data/fluxnet_status_table.csv]
    --info-dir=<dir>  Directory to store site data in  [default: data/fluxnet_info/]
    <site_id>         Fluxnet site id, e.g. EE-Aar, CA-Let, etc.
"""

from docopt import docopt

import os
import sys
import requests
import bs4
import pandas as pd
import yaml

from collections import OrderedDict

from pals_utils import setup_logger
logger = setup_logger(__name__, 'logs/fluxnet_scraper.log')


def scrape_fluxnet_status_table(csv_file):
    if os.path.exists(csv_file):
        logger.warning("Warning, %s exists, overwriting" % csv_file)
    table_url = "http://fluxnet.ornl.gov/site_status"
    soup = bs4.BeautifulSoup(requests.get(table_url).text, 'lxml')

    table = soup('table', {'id': 'historical_site_list'})[0]
    links = table.find_all('a')
    ID_table = pd.DataFrame.from_records(
        [[a.contents[0], a.get('href').replace('site/', '')] for a in links])
    ID_table.columns = ['Site Name', 'table_id']

    df = pd.read_html(str(table))[0].replace(pd.np.nan, 0)
    df.columns = (list(df.columns[0:2]) +
                  ['19%s' % c if c.startswith('9') else '20%s' % c for c in df.columns[2:]])
    table = pd.merge(df, ID_table)

    table.to_csv(csv_file)

    return


def table_rows_to_dict(table_rows):

    info = OrderedDict()

    for tr in table_rows:
        info[tr("td")[0].get_text().rstrip(': ')] = tr("td")[1].get_text()

    return info


def get_fluxnet_site_data(site_id):

    url = "http://fluxnet.ornl.gov/site/%s" % str(site_id)
    soup = bs4.BeautifulSoup(requests.get(url).text, 'lxml')

    info = OrderedDict()

    # main info tables
    tables = soup.find_all("table", {"class": "fluxnet_site_table"})
    for table in tables:
        try:
            info[table("tr")[0].get_text()] = table_rows_to_dict(table("tr")[1:])
        except IndexError:
            # Skip the disclaimer table
            if table("tr")[0]("th")[0].get_text() == ("Tower Site Information Disclaimer"):
                pass
            else:
                raise Exception("Unexpected table: %s" % table.get_text())

    # Data collection timeframe table
    for table in soup.find_all("table", {"class": None}):
        try:
            info.update(table_rows_to_dict(table("tr")[0:1]))
        except:
            import ipdb
            ipdb.set_trace()

    return info


def get_status_table(csv_file):
    try:
        status_table = pd.read_csv(csv_file)
    except OSError:
        logger.exception("Site status table ({csv}) not available, run `Fluxnet_metadata_scrape.py get_table` first.".format(csv=csv_file))
        sys.exit(1)

    return status_table


def quit_bad_site_id(site_id):
    logger.critical("Unknown site id ({site}), might need to update your metadata table with `Fluxnet_metadata_scrape.py get_table`.".format(site=site_id))
    sys.exit(1)


def scrape_fluxnet_site_page(site_id, csv_file, info_dir):
    status_table = get_status_table(csv_file)

    try:
        # using a numeric ID
        int(site_id)
        try:
            fluxnet_id = status_table[status_table["table_id"] == site_id]["FLUXNET ID"].values[0]
        except IndexError:
            quit_bad_site_id(site_id)
    except ValueError:
        # Using a fluxnet ID
        try:
            fluxnet_id = site_id
            site_id = status_table[status_table["FLUXNET ID"] == fluxnet_id]["table_id"].values[0]
        except IndexError:
            quit_bad_site_id(site_id)

    scrape_and_save(site_id, fluxnet_id, csv_file, info_dir)

    return


def scrape_and_save(site_id, fluxnet_id, csv_file, info_dir):
    info = get_fluxnet_site_data(site_id)

    os.makedirs(info_dir, exist_ok=True)
    out_file = os.path.join(info_dir, fluxnet_id + '.yaml')
    if os.path.exists(out_file):
        logger.warning("Warning: {f} exists, overwiting.".format(f=out_file))
    with open(out_file, 'w') as f:
        logger.info("Writing data to {f}.".format(f=out_file))
        yaml.dump(info, f)

    return


def scrape_all_fluxnet_sites(csv_file, info_dir):
    status_table = get_status_table(csv_file)

    for i, row in status_table.iterrows():
        fluxnet_id = row['FLUXNET ID']
        site_id = row['table_id']
        scrape_and_save(site_id, fluxnet_id, csv_file, info_dir)

    return


def main(args):

    if args['get_table']:
        scrape_fluxnet_status_table(csv_file=args['--csv-file'])
    elif args['get_site']:
        scrape_fluxnet_site_page(args['<site_id>'],
                                 csv_file=args['--csv-file'],
                                 info_dir=args['--info-dir'])
    elif args['get_all_sites']:
        scrape_all_fluxnet_sites(csv_file=args['--csv-file'],
                                 info_dir=args['--info-dir'])
    return


if __name__ == '__main__':
    args = docopt(__doc__)

    main(args)
