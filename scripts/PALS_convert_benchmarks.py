#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
File: PALS_convert_benchmarks.py
Author: ned haughton
Email: ned@nedhaughton.com
Github: https://github.com/naught101
Description:
    converts PALS benchmarks from the old 3-benchmarks-per file to a file per benchmark

Usage:
    PALS_convert_benchmarks.py
"""

from docopt import docopt

import os

from pals_utils.data import get_pals_old_benchmark
from pals_utils.constants import DATASETS

from pals_utils import setup_logger
logger = setup_logger(__name__, 'logs/fluxnet_scraper.log')


def main(args):

    new_data_path = 'data/PALS/benchmarks/{name}'
    new_file_path = 'data/PALS/benchmarks/{name}/{name}_{site}Fluxnet.1.4.nc'

    benchmark_names = ['1lin', '2lin', '3km27']

    for name in benchmark_names:
        data_dir = new_data_path.format(name=name)
        if not os.path.exists(data_dir):
            os.makedirs(data_dir)

    for site in DATASETS:
        print('Converting benchmarks for %s:' % site, end=' ')
        for name in benchmark_names:
            benchmark = get_pals_old_benchmark(name, site)

            print(name + ',', end=' ')
            logger.info('Converting benchmarks for %s for %s' % (site, name))
            benchmark.to_netcdf(new_file_path.format(name=name, site=site))
        print('')

    return


if (__name__ == '__main__'):
    args = docopt(__doc__)

    main(args)
