#!/usr/bin/env python

from distutils.core import setup

setup(name='pals_utils',
      version='0.01',
      description='PALS helper package',
      author='ned haughton',
      author_email='ned@nedhaughton.com',
      url='http://www.unknown.com/not_there',
      packages=['pals_utils', 'pals_utils.munge'],
      package_data={'': ['data/*.yaml']},
      )
